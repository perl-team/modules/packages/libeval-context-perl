libeval-context-perl (0.09.11-7) unstable; urgency=medium

  * Team upload.
  * Add patch from CPAN RT for Perl 5.40 compatibility. (Closes: #1078044)
  * Add another patch to fix warnings with Perl 5.40.
    Second part of the fix for #1078044.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Aug 2024 18:20:50 +0200

libeval-context-perl (0.09.11-6) unstable; urgency=medium

  * annotate test-only build-dependencies with <!nocheck>
  * add a patch fixing test suite with libdata-treedumper-perl 0.41.
    Also bump the build-dependency (Closes: #1056143)
  * declare conformance with Policy 4.6.2 (no changes needed)
  * patch a couple of spelling mistakes in documentation

 -- Damyan Ivanov <dmn@debian.org>  Sun, 26 Nov 2023 15:55:30 +0000

libeval-context-perl (0.09.11-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libeval-context-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 13:43:49 +0000

libeval-context-perl (0.09.11-4) unstable; urgency=medium

  [ Axel Beckert ]
  * Use uptodate upstream e-mail also in debian/copyright's
    Upstream-Contact header. Issue found by DUCK.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description-synopsis warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:40:46 +0100

libeval-context-perl (0.09.11-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 17:52:39 +0100

libeval-context-perl (0.09.11-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Update Nadim Khemir's e-mail address in debian/copyright.
    Issue reported by DUCK.

  [ gregor herrmann ]
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Bump debhelper compatibility level to 9.
  * Unconditionally build depend on libmodule-build-perl.
  * Drop some ancient versions from perl (build) dependencies.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 May 2015 18:32:00 +0200

libeval-context-perl (0.09.11-2) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request
    (https://lists.debian.org/debian-perl/2013/08/msg00053.html)
  * debian/control: Changed: Homepage field changed to metacpan.org URL;
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: David Paleino
    <dapal@debian.org>); put myself to Uploaders.
  * debian/watch: use metacpan-based URL.
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * migrate Vcs-* headers
  * Standards-Version: 3.9.4 (no changes needed)
  * add hash-randomization.patch fixing build failure with perl 5.18
    Closes: 711442

 -- Damyan Ivanov <dmn@debian.org>  Tue, 13 Aug 2013 16:10:26 +0200

libeval-context-perl (0.09.11-1) unstable; urgency=low

  * Initial Release (Closes: #574615)

 -- David Paleino <dapal@debian.org>  Wed, 14 Apr 2010 18:27:40 +0200
